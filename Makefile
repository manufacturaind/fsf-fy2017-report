DEBUG = True
BUILD_DIR = dist

SSH_HOSTNAME = dh
SSH_DIR = /home/rlaf/www/manufacturaindependente.org/fsf-report/
SSH_PATH = $(SSH_HOSTNAME):$(SSH_DIR)

SASS_FILES_DIR = static/css
JS_FILES_DIR = static/js
FONTS_DIR = static/fonts
IMAGES_DIR = static/images
CONTENT_IMAGES_DIR = content/images
JS_VENDOR_FILES_DIR = static/js/vendor
TARGET_CSS_DIR = $(BUILD_DIR)/css
TARGET_JS_DIR = $(BUILD_DIR)/js
TARGET_FONTS_DIR = $(BUILD_DIR)/fonts
TARGET_IMAGES_DIR = $(BUILD_DIR)/images

# CSS via Sass
SASS_FILES = $(wildcard $(SASS_FILES_DIR)/*.scss)
CSS_FILES=$(patsubst $(SASS_FILES_DIR)/%.scss,$(TARGET_CSS_DIR)/%.css,$(SASS_FILES))
SASS=./lib/sass
ifeq ($(DEBUG),True)
    SASS_FLAGS = --style expanded
else
    SASS_FLAGS = --style compressed --quiet
endif

# helper python commands
ACTIVATE=. `pwd`/.env/bin/activate

build: dir css js fonts images html

dir:
	mkdir -p $(BUILD_DIR) $(TARGET_CSS_DIR) $(TARGET_JS_DIR) $(TARGET_FONTS_DIR) $(TARGET_IMAGES_DIR)

css: $(CSS_FILES)
$(TARGET_CSS_DIR)/%.css: $(SASS_FILES_DIR)/%.scss
	$(SASS) $(SASS_FLAGS) $< > $@

js:
	# cp $(JS_FILES_DIR)/*.js $(TARGET_JS_DIR)
	cp $(JS_VENDOR_FILES_DIR)/*.js $(TARGET_JS_DIR)

fonts:
	cp $(FONTS_DIR)/* $(TARGET_FONTS_DIR)

images:
	cp $(IMAGES_DIR)/* $(TARGET_IMAGES_DIR)
	cp $(CONTENT_IMAGES_DIR)/* $(TARGET_IMAGES_DIR) -r

html:
	$(ACTIVATE); python3 lib/jinja_generate.py
	cp print-fsf-fy2017.pdf dist/

serve:
	make -j2 runserver watch
runserver:
	cd dist; python -m SimpleHTTPServer
watch:
	watch make
clean:
	rm -fr $(BUILD_DIR)
install:
	virtualenv .env --python=/usr/bin/python3 --no-site-packages --distribute
	$(ACTIVATE); pip install -r requirements.txt


deploy:
	rsync --compress --checksum --progress --recursive --update dist/ $(SSH_PATH)
dry-deploy:
	rsync --dry-run --compress --checksum --progress --recursive --update dist/ $(SSH_PATH)

.PHONY: clean install watch serve html css js dir
