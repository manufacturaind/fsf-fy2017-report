#!/usr/bin/env python3
import jinja2
import markdown
import os

TEMPLATES_PATH = 'templates/'
CONTENT_PATH = 'content/sections/'


def render_template_into_file(env, templatename, filename, context=None):
    template = env.get_template(templatename)
    if not context:
        context = {}
    html = template.render(**context)
    with open(filename, "w") as fh:
        fh.write(html)


env = jinja2.Environment(loader=jinja2.FileSystemLoader([TEMPLATES_PATH]),
                         extensions=[
                             'jinja2.ext.with_'],
                         trim_blocks=True,
                         lstrip_blocks=True)


def read_markdown_file(filename):
    md = markdown.Markdown(extensions=['meta', 'tables'])
    html = md.convert(open(filename, 'r').read())
    result = {}
    result.update(md.Meta)
    # each meta field value is a list (to preserve linebreaks, see
    # https://python-markdown.github.io/extensions/meta_data/ )
    # we don't want this, so extract the single items
    for key in result:
        result[key] = result[key][0]
    result['content'] = html
    return result


if __name__ == "__main__":
    context = {}
    # add all content files to a context as first-class objects
    for filename in os.listdir(CONTENT_PATH):
        if not filename.endswith('.md'):
            continue
        title = filename.replace('.md', '')
        context[title] = read_markdown_file(os.path.join(CONTENT_PATH, filename))
    render_template_into_file(env, 'index-web.html', 'dist/index.html', context)
    render_template_into_file(env, 'index-print.html', 'dist/print.html', context)
