### FY2017 at a glance

* **331** copyright assignments and disclaimers accepted 
* **670** licensing questions fielded from the public
* **54** reports of GNU license violations investigated 
* **18** devices certified to Respect Your Freedom
* **1.8 million** visitors to the Free Software Directory

