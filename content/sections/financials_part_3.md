### Statement of Activities

| Support and Revenue            |                 | 
|--------------------------------|-----------------| 
| Contributions                  |  $1,294,697     | 
| In-Kind Contributions          |  $3,600         | 
| Earned Revenue                 |  $70,406        | 
| Interest and Other Income      |  $8,471         | 
| Gain/(Loss) on Investments     |  ($14,899)      | 
| **Total Support and Revenue**  |  **$1,362,275** | 

| Functional Expenses           |                 | 
|-------------------------------|-----------------| 
| Program Services              |  $1,076,394     | 
| Management and General        |  $95,105        | 
| Fundraising                   |  $65,639        |
| **Total Functional Expenses** |  **$1,236,994** |
| _Change in Net Assets_        |  _$125,281_     | 
