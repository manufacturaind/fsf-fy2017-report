# ANNUAL REPORT TEXT DRAFT

## PAGE: COVER

Free Software Foundation 
FY2017 Annual Report
[**small font: Highlighting activities and detailed financials for Fiscal Year 2017 (October 1, 2016 - September 30, 2017)]

THE FOLLOWING TEXT GOES IN THREE BOXES AT BOTTOM OF PAGE

### Licensing

Educating you about copyleft and enforcing the GNU General Public
License

### Campaigns

Empowering you to understand, adopt, develop, and defend free software

### Tech

Providing the infrastructure that undergirds your GNU Project
contributions



## PAGE: INSIDE FRONT COVER

IMAGE, full or half page: (suggest using: https://static.fsf.org/nosvn/photos/title2_protest-boston-11-17.png)

TEXT: Toward a libre planet



## SECTION: PREAMBLE PAGE 1

The Free Software Foundation is a leader in the international movement
for computer user freedom. We defend the rights of all software users
and encourage the development and use of free software.

This annual report highlights the Foundation's activities and
achievements in fiscal year 2017 (October 1, 2016 - September 30,
2017), as well as a detailed financial statement.



## SECTION: ED LETTER / PAGES: 2-3

TEXT: [**updated] LETTER FROM EXECUTIVE DIRECTOR

Dear Supporters,

As I write this, reflecting on our previous financial year,
discussions centered on our rights and our fears while using
technology are all over the mainstream news. Facebook is being called
before governments in the US and Europe to explain itself. Uber and
Tesla are under fire for their software-driven cars killing
people. Investigations continue into how criminals may have broken
into voting machines and used online news platforms to influence
elections in the US and elsewhere around the world. Pundits say people
are more divided than ever on "the issues," and that we need to
actualize the theoretical capability of electronic communication to
bridge differences and bring people together.

The common but silent thread connecting these discussions is the power
of software. Software filters the information we receive about the
world, the messages we put out into the world, and even the way we
physically move in the world. If the software is not free "as in
freedom," so that anyone can inspect, modify, and share it, these
filters will inevitably be keyed to the interests of the gatekeepers
who control it. The consequences for the rest of us will be loss of
democracy, privacy, security, freedom of speech, freedom of movement
-- and even loss of life. And yet, the ideals of the free software
movement have been largely suppressed in the global conversation about
avoiding technological dystopia.

The FSF is changing that. The audited FY2017 financial statement
contained in this report continues the solid good news you've come to
expect from us -- the steady growth and transparent, mission-centric
use of funds that have earned the FSF an exceptionally high **[Charity
Navigator
rating](https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=8557)
of 97.15 out of 100.

In these pages, you'll see how we've been expanding the free software
movement and amplifying your principled voice on the international
stage, accelerating development of the GNU operating system and other
free software, inspecting and certifying freedom-respecting products
to incentivize needed change in the commercial sector, and
strengthening copyleft licensing -- still the best tool we have to
establish a culture where technology empowers rather than oppresses. I
hope that these program updates from our campaigns, technical,
licensing, and operations teams inspire you to step up your own work
toward our shared goals.

We need everyone not only using free software in their daily lives but
also seeing free software as their political, philosophical issue. We
will eventually reach a tipping point, where awareness of the Four
Freedoms will be sufficiently ingrained in policymakers,
technologists, and the public, such that any new enterprise that does
not respect them will meet quickly with failure. But we have a long
way to go, and delays in getting there have real, human costs.

FSF associate members drive this work, through their ongoing financial
contributions, their commitment to the movement, and their sustained,
informed, effective personal advocacy. If you haven't joined us yet,
we need your support. We may be up against billions of dollars, but
since we are fighting for billions of regular users, we have a massive
advantage in numbers -- so long as we can reach everyone. Please join
us and help build these freedom-protecting defenses, without delay.

Yours in freedom,
[SIGNATURE]  
John Sullivan  
Executive Director  
June 2018  

PHOTO: JohnS.jpg

PULL QUOTE: Software filters the information we receive about the
world and the messages we put out into the world, and that software
must be free "as in freedom."



## SECTION: TOC PAGE 4

TEXT: For print version - section head names (for example: LICENSING & COMPLIANCE) and accompanying page numbers for all sections following the TOC

For web version - section head names that link to the section

PHOTO: https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-84/

CREDIT: Kori Feener, CC BY 4.0

CAPTION: Free software community members at [**LibrePlanet
2017](https://libreplanet.org/2017/) (IF FURTHER DESCRIPTIVE TEXT IS
USED: six people, smiling and wearing lanyards, plus a dog wearing a
red t-shirt)

## SECTION: LICENSING & COMPLIANCE / PAGES 9-11

The FSF's [**Licensing and Compliance
Lab](https://www.fsf.org/licensing/) uplifts and defends free software
through software license administration and defense, rigorous
certifications, and educational resources.

In FY17, the [**GNU General Public License
(GPL)](https://www.gnu.org/licenses/gpl.html), the free software
copyleft license that ensures the user's right to run, study, share,
and modify software, celebrated the tenth anniversary of GPLv3. The
FSF holds copyright on much of the GNU operating system, and the
licensing team spent the year accepting copyright assignments from
software developers and corporations, investigating GPL violations,
and answering licensing questions from the community.

We continued to raise the alarm about legislative issues like the
U.S. Copyright Office's continued insistence on [**forcing supporters
of user freedom to fight for exemptions to the Digital Millennium
Copyright Act's (DMCA) anti-circumvention
restrictions](https://www.defectivebydesign.org/blog/tell_us_copyright_office_dmca_anticircumvention_rules_are_broken). The
DMCA is a particularly egregious example of legislative endorsement of
[**Digital Restrictions Management
(DRM)](https://www.defectivebydesign.org/what_is_drm_digital_restrictions_management),
which locks down digital media, preventing users from using digital
media as they please -- even the process for getting exceptions to the
anti-circumvention rules for the purposes of research or use of
assistive technologies is draconian, resetting every three years and
requiring nonfree JavaScript in order to submit a comment on the
process, unless you get special permission to comment in another way
(**and the FSF worked with volunteers to [come up with a workaround
for
that](https://www.fsf.org/blogs/licensing/you-can-now-register-as-a-dmca-agent-without-using-nonfree-javascript)). Along
with our own comments, we rallied the free software community to
submit their own comments in favor of anti-circumvention exemptions --
and passionate critiques of the entire process.

We championed copylefted projects, publishing another seven
installments in ongoing series of conversations with free software
developers who choose GNU licenses for their work. We certified a
record number of hardware devices under our [**Respects Your
Freedom](https://www.fsf.org/ryf) certification program, which
identifies devices that do as much as possible to respect user
freedom.

Along with answering hundreds of licensing questions from the public,
dedicated volunteers continued to expand and improve the [**Free
Software
Directory](https://www.fsf.org/resources/hw/endorsement/respects-your-freedom)
for its millions of annual visitors, and we made inroads with
government, helping the U.S. Department of Defense to distribute free
software.

[OPTIONAL PULL QUOTE: We champion copylefted projects and developers who choose GNU licenses for their work.]

[BOX:

Licensing & Compliance FY17

accepted 331 copyright assignments and disclaimers
fielded 670 licensing questions from the public
investigated 54 reports of GNU license violations
certified 18 devices to Respect Your Freedom
saw 1.8 million visitors to the Free Software Directory]

[BOX:

Respects Your Freedom certification

The FSF's Respects Your Freedom computer hardware product
certification program encourages the creation and sale of hardware
that will do as much as possible to respect your freedom and privacy,
and ensure that you have control over the device.

In FY17, fifteen devices from Technoethical became RYF certified: six
laptops, two docking stations, a mainboard, three WiFi USB adapters,
two internal WiFI devices, and a Bluetooth USB adapter. RYF
certification was also awarded to three devices from Vikings: a USB
stereo sound adapter, a mainboard, and a laptop, bringing the total
number of RYF certified devices to twenty seven (at the time).

IMAGE: RYF logo: https://static.fsf.org/nosvn/ryf/RYF-with-fill.png]

IMAGE: https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-61/

CAPTION: Denver Gingerich, a developer of several GNU licensed software packages, speaking at LibrePlanet 2017 about free software for mobile phones. (For accessibility description add: photo of a man with light brown hair, glasses, and a brown, white, and red sweater, holding two small mobile phones in his hands.)

CREDIT: Kori Feener, CC BY 4.0



## SECTION: **TECH 12-14

### Infrastructure for free software development and activism

This year, the FSF's sysadmin team made some crucial infrastructure
upgrades that improve both the Foundation's daily operations and the
[**GNU Project](https://www.gnu.org/). The changes include running
more services on hardware that is Respects Your Freedom certified, has
a Librebooted BIOS, and runs Trisquel GNU/Linux, proving that complex
software projects and modern nonprofit organizations can succeed using
free software exclusively. Much of the software the FSF uses was also
upgraded.

The sysadmin team and its volunteers also power LibrePlanet, the
annual free software conference. Its livestream and recording
infrastructure reduce barriers to access for those who are not able to
attend the conference in person. Volunteer monitored IRC channels for
each talk and workshop room, increasing the opportunity to contribute to
the conversation. Recordings are archived on the FSF's [**GNU
MediaGoblin instance](https://media.libreplanet.org/_ shortly after
the event. And it's all done with free software!

**Additionally, this three-person team runs the GNU shell server,
oversees the security of GNU software distribution and
infrastructure, and works with volunteers to maintain
savannah.gnu.org. They also maintain many services used by staff, volunteers, and the free software community, including the FSF's CiviCRM server, fsf.org, libreplanet.org, defectivebydesign.org, the Free Software Directory, XMPP servers for staff and members, mailing lists, apt mirrors, git repositories, internal wikis, a GNU social server, a Single Sign-On server, email servers, DNS servers, internal site monitoring systems, a Request Tracker instance, and the FSF shop. 

And the team collaborates with the free software community, maintaining gnu.org with the help of volunteers, and working with interns who are advancing their skills and knowledge in working with and creating free software.

The team has introduced some new infrastructure and performed multiple upgrades in the past year. They started building our "gnuhope" infrastructure using distributed Ceph storage and multiple KVM hosts for new virtual machines, upgraded our StatusNet server to GNU social, upgraded our XMPP servers used by staff and members, upgraded the software running the Free Software Directory and the Libreplanet wikis, moved one of our internal wikis to use git instead of svn, kept our Drupal servers up to date, upgraded our CiviCRM server, improved our CAS server, and upgraded our Request Tracker to a new major release.They maintain and apply updates to the FSF's CiviCRM server, and maintain many services for internal and external use, including fsf.org, libreplanet.org, The FSF tech team is small, but mighty!

The team has already begun the next stage of infrastructure upgrades, including the implementation a new mail server
stack supporting over three thousand free software project mailing lists, and improving the LibrePlanet conference streaming setup by using Ansible to manage the laptops used for streaming, introducing new hardware, and using HUBAngl (streaming software created by an FSF intern). 

[OPTIONAL PULL QUOTE: The FSF has upgraded many of the free software services the organization and the free software community rely on.]

[Box: 

FSF Sponsored Projects

The FSF acts as fiscal sponsor for free software projects whose scope
and purpose align with the FSF’s mission. Participating projects
benefit from the FSF’s nonprofit status, administrative
infrastructure, and fundraising expertise. In FY17, these eight
projects received $67,130.136 in direct support from the free software
community via the Working Together for Free Software Fund.

* [**GNU Guix](https://www.gnu.org/software/guix/)
* [GNU Mailman](https://www.gnu.org/software/mailman/)
* [GNU MediaGoblin](https://www.mediagoblin.org/)
* [GNU Octave](https://www.gnu.org/software/octave/)
* [GNU Radio](https://www.gnuradio.org/)
* [GNU Toolchain](https://gcc.gnu.org/) **please add the words "GNU Toolchain" accompanying the GNU head image
* [Replicant](https://www.replicant.us/)
* [SeaGL](http://seagl.org/)

(note: can display using logos or in another way – probably not a bulleted list!)]

[BOX

Defending user freedom with free software

FSF and GNU infrastructure includes:

* over 100 virtual machines
* 11 physical machines
* 3 data centers & some in-house hosting
* 396 volunteer maintainers

What's GNU in FY17

* ~400 GNU packages
* 234 new GNU releases
* 432K emails per day on lists.gnu.org and lists.nongnu.org
* average of 1M unique visitors to gnu.org per month]

PHOTO: https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-49/

CAPTION: The sysadmin team maintains the free software infrastructure for all FSF operations, including the annual LibrePlanet conference.

(Accessibility description: a tiered classroom, filled with people. A person stands at the front of the room, beneath a large projection screen, giving a talk.)

CREDIT: Kori Feener, CC BY 4.0



## SECTION: CAMPAIGNS 5-8 (INC LP, RMS, STATS ON EVENTS)

## Fighting DRM wherever it's found

In the past year, we've seen, and fought back, against Digital Restrictions Management (DRM) on multiple fronts.

### International Day Against DRM

[**add photo https://media.libreplanet.org/u/libreplanet/m/34th-street-wall-gainsville-florida - pull caption and credit from here, as well]

This year, [**International Day Against DRM
(IDAD)](https://www.defectivebydesign.org/dayagainstdrm/) focused on
Web-based community involvement, with more than 20 organizations
raising their voices in support of a world without DRM. From blog
posts to special sales to parties and movie showings around the world,
people came out to celebrate DRM free media, and raise awareness of
the threats we face in DRM.

### EME in Web standards

Encrypted Media Extensions (EME) is just another way to dress up
DRM. While the World Wide Web consortium (W3C) was in turmoil over the
divisive proposal of turning EME (and therefore DRM) into a Web
standard, the FSF and our members and constituents were organizing
against EME. We called and wrote W3C president Tim Berners-Lee, asking
him to keep the Web free. [**While we lost this
battle](https://www.defectivebydesign.org/blog/tim_bernerslee_approves_web_drm_w3c_member_organizations_have_two_weeks_appeal),
and EME became a Web standard, we're not giving up hope for a free
Web.

## Free people, free net

Internet freedom in the United States found itself on the national
stage in 2017. Over the course of the year, Ajit Pai became chairman
of the Federal Communication Commission (FCC) and instigated a war
against net neutrality. The FSF [**joined with dozens of other
organizations to raise awareness and organize in support of net
neutrality](https://www.fsf.org/blogs/community/today-july-12th-day-of-action-for-net-neutrality).

# stats

**Free Software Supporter subscribers:
     * October, 2016: 119,299
     * September, 2017: 159,068

LibrePlanet **2017:
     * 353 attendees
     * 56 sessions
* 32 hours of streamed and recorded videos

[**use this photo with the LP stats: https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-84/ pull caption from there as well]



## PAGE: **LEADERSHIP & STAFF

[Arrange this in a box]

Board of Directors

Richard M. Stallman, Founder and President [USE PHOTO: https://static.fsf.org/fsforg/img/rms-web.jpg]
Gerald J. Sussman, Professor of Electrical Engineering, MIT
Geoffrey Knauth, Computer Science Instructor at Lycoming College
Henry Poole, Founder, CivicActions
Benjamin Mako Hill, Assistant Professor of Communications at the University of Washington
Bradley M. Kuhn, President, Software Freedom Conservancy
Kat Walsh

[Arrange this in a box]

Leadership

John Sullivan, Executive Director
John Hsieh, Deputy Director [USE PHOTO https://static.fsf.org/fsforg/img/johnh.jpg]

Operations

Jasimin Huang, Business Operations Manager
Matt Lavallee, Operations Assistant [USE PHOTO https://static.fsf.org/fsforg/img/mattlav.jpg]
Jeanne Rasata, Assistant to the President

Sysadmin

Andrew Englebrecht, Web Developer [USE PHOTO https://static.fsf.org/fsforg/img/andrew.png]
Ian Kelling, Senior Systems Administrator [USE PHOTO https://static.fsf.org/nosvn/images/staff-photos/ian-headshot-small.jpg]
Ruben Rodriguez, Senior Systems Administrator [USE PHOTO https://static.fsf.org/fsforg/img/ruben.jpg]

Campaigns

Molly de Blanc, Campaigns Manager [USE PHOTO https://static.fsf.org/nosvn/images/staff-photos/molly-headshot.jpg]
Dana Morgenstein, Outreach & Communications Coordinator [USE PHOTO https://static.fsf.org/nosvn/images/staff-photos/dana-headshot-sm.JPG]
Georgia Young, Program Manager [USE PHOTO https://static.fsf.org/fsforg/img/georgia-new.jpg]

Licensing

Donald Robertson, Licensing & Compliance Manager [USE PHOTO https://static.fsf.org/fsforg/img/don-new.jpg]
Ted Teah, Copyright and Licensing Associate

[smaller text: Leadership & staff lists are up to date as of publication, August 2018.]

[staff and RMS photos available here https://www.fsf.org/about/staff-and-board/]

## PAGE: FINANCIALS 15-17

PDF: FINANCIAL STATEMENT
FY2017 Audited Financial Statement 
Download PDF

## PAGE 18-21: DONORS

SECTION OPENER IMAGE

### Institutional Support

Private Internet Access  
Alibaba Group  
Craigslist Charitable Fund  
2A Foundation  
Bloomberg  
Google  
Skowronski Family Foundation  
Bibliotek-Systemer AS  
IUT Béziers (Université Montpellier 2)  
Purism  
Audeo  
GitHub  
Open Invention Network  

### In-Kind Support

Bytemark  
Markley Group  
TowardEX  
Technoethical  
Aleph Objects  
No Starch Press  
ThinkPenguin  

### $5000+

**Cristian Francu    
Julian Graham  
Gregory Maxwell  
James Wilson  

### $1000+

Aleph Objects, Inc  
Jean-Francois Blavier  
Alain Brenzikofer  
Shawn C [ a.k.a “citypw”]  
Colin Carr  
Antonio Carzaniga  
Jeffrey Cliff  
Steven Dick  
Robert Dionne  
Alexey Eromenko  
Cătălin Frâncu  
Matteo Frigo
René Genz  
Richard Harlow  
Douglas Hauge  
Matthias Herrmann
Jonathan Howell  
Stephen Ippolito  
Brewster Kahle
Donald and Jill Knuth
Nikolay Ksenev
Russell McManus
Trevor Menagh
Nebion AG
Seungwon Park
Sreeram Ramachandran
Daniel Riek
Peter Rock
Luis Rodriguez
Inouye Satoru
Steve Sprang
John Sullivan
Puduvankunnil Udayakumar
C&CZ IT Department, Faculty of Science, Radboud University
Kat Walsh
Philipp Weis
Marinos Yannikos

### $500-999

Jean-Louis Abraham
Ben Abrams
Bashar Al-Abdulhadi
Xavier ALT
Iñaki Arenaza in memory of Mr. Mauricio Saint-Supery
Matthew Armstrong
AskApache
Salim Badakhchani
Alexandre BLANC
Blue Systems
Mark Boenke
Wade Brainerd
Nicolae Carabut
Alison Chaiken
Conan Chiles
Yidong Chong
Judicaël Courant
Donald Craig
Allen Curtis
Dangerous Thing
Henrique Dante de Almeida
Paul Eggert
Markus Fischer
Edward Flick
Andrew Fox
Arthur Gleckler
Elyse Grasso
Aaron Grothe
Sam Halliday
Steven Hay
Michael Henderson
Håkon A. Hjortland
Brett Holleman
Daniel Hoodin
Clifford Ireland
Martin Jansche
Christian Johansen
Chase Kelley
David Klann
Colin Klingman
Adam Klotblixt
Warren Knight 
Øyvind Gard Knudtzen
Martin Krafft
Peter Kunze
Adam Lewis
Morten Lind
Denis López
Shyama Mandal
Christopher Marusich
Miromico AG
David Moews
Kyohei Moriyama
Bill Newcomb
Pablo Adrian Nieto
Freddie O'Connell
jeffrey oconnell
Stephanie Ogden
Marcus Pemer
Donnie Pennington
Roland Pesch
Valerio Poggi
David Potter
Nicolas Pottier
Ed Price
Vivek Ramachandran
Norman Richards
francisco rodriguez
Tyler Romeo
Leah Rowe
Sean Russell
Minoru Sekine
Ben Simmonds
Bijan Soleymani
Trevor Spiteri
Gary Stimson
悟高田
Micah Tombli
David Turner
Rob Vens
Spencer Visick
Paul Wang
ivo Welch
Eric West
Jim Wright
hiroo yamagata
Adam Ymeren

**This list includes our patrons, in-kind supporters, and those who
  receive ThankGNUs for donations totaling over $500 in a year. The
  FSF appreciates and thanks the thousands of individual donors,
  members, and corporate patrons worldwide who make our work possible.

## PAGE 22: FSF MEMBERSHIP

Join the Free Software Foundation

Our mission is to promote computer user freedom and defend the rights
of all software users, worldwide. Though free software is used more
widely than ever, it is under threat from a wide range of interests
that profit from controlling and surveiling computer users. You can
help put control over computers back in the hands of the people who
use them! Try [a free software
program](https://directory.fsf.org/wiki/Free_Software_Directory:Free_software_replacements),
or [switch to a free operating
system](https://www.gnu.org/distros/free-distros.html). [Make a
donation](https://my.fsf.org/donate), or make a long term commitment
to free software by [becoming a Free Software Foundation Associate
Member](https://my.fsf.org/join). Learn more at <fsf.org>.

When people become FSF Associate Members, we ask them why they decided
to support free software in this way. Here are a few reasons:

"Free software--the GNU/Linux platform in particular--is what made it
possible 20 years ago for me to learn the skills to become a
professional system administrator. It has been a rewarding career!"

"Free software positively impacts my life every hour of every day."

"The dystopia of a DRM-overgrown world."

"I simply use GNU tools every day."

"Discovering GNU, then learning about all the amazing work the FSF
does for the tech community."



## PAGE 23: INSIDE BACK COVER

**PHOTO: https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-84/ [in web version, this can accompany the membership text in the previous section]

## PAGE 24: BACK COVER

???? Something back cover-y

## USEFUL PHOTOS

* IDAD 2017, Gainesville, FL https://media.libreplanet.org/u/libreplanet/m/34th-street-wall-gainsville-florida/ (campaigns)

* FSF staff at LP 17 https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-87/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-41/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-39/ (campaigns or staff pic)
* Sumana Harehareswara, LP 17 keynote speaker https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-84/ (campaigns)
* LP 17 roots of freedom t-shirt https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-72/
* women at LP (mostly speakers) https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-68/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-66/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-62/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-51/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-44/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-38/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-26/
* male speakers at LP https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-61/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-9/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-47/
* people at LP https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-33/, **https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-32/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-19/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-10/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-8/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-85/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-84/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-80/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-friday-12/
* people using free software https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-sunday-53/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-66/, https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-18/
* photos of rms https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-96/
* free software awards https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-99/
* john sullivan at lp https://media.libreplanet.org/u/libreplanet/m/libreplanet-2017-photo-saturday-14/
* Net Neutrality protest https://static.fsf.org/nosvn/photos/title2_protest-boston-11-17.png
