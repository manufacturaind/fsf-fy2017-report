#### Why I joined the FSF

> "Free software — the GNU/Linux platform in particular — is what made it possible twenty years ago for me to learn the skills to become a professional system administrator. It has been a rewarding career!"
