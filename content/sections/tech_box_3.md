#### What's GNU in FY17

* **~400** GNU packages
* **234** new GNU releases
* **432K** emails per day on lists.gnu.org and lists.nongnu.org
* **average of 1.2M** monthly unique visitors to gnu.org
