### Respects Your Freedom certification

<p class="left">
  <a href="https://www.fsf.org/ryf" target="_blank"><img alt="RYF logo" src="images/RYF-logo.png"></a>
</p>

The FSF's [Respects Your Freedom](https://www.fsf.org/ryf) product
certification program encourages the creation and sale of hardware
that will do as much as possible to respect your freedom and privacy.

In FY17, fifteen devices from Technoethical became RYF certified: six
laptops, two docking stations, a mainboard, three WiFi USB adapters,
two internal WiFI devices, and a Bluetooth USB adapter. RYF
certification was also awarded to three devices from Vikings: a USB
stereo sound adapter, a mainboard, and a laptop, bringing the total
number of RYF certified devices to twenty-seven (at the time).



