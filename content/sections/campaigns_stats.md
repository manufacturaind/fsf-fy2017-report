#### Free Software Supporter subscribers

  * **119,299** October, 2016
  * **159,068** September, 2017

![Sumana Harihareswara gives the closing keynote at LibrePlanet 2017](images/campaigns-stats-lp_small.jpg)
<p class="image-caption">
  <small class="caption">Sumana Harihareswara gives the closing keynote at LibrePlanet 2017</small>
  <small class="credit">Credit: Kori Feener, CC-BY 4.0</small>
</p>
