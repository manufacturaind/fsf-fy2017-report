Title: Footer

Copyright © 2018 [Free Software Foundation](https://u.fsf.org/ys), Inc. 
[Privacy Policy](https://www.fsf.org/about/free-software-foundation-privacy-policy).  
Please support our work by [joining us as an associate member](https://my.fsf.org/join).

Download the [source package]() for the report including fonts, image source
files and text. This work is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).
