#### Defending user freedom with free software

FSF and GNU infrastructure includes:

* **over 100** virtual machines
* **11** physical machines
* **3** data centers & some in-house hosting
* **396** volunteer maintainers
* **0** Amazon EC2 instances
