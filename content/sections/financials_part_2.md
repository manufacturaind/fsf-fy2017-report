### Liabilities and Net Assets

| Liabilities                      |                 | 
|----------------------------------|-----------------| 
| Accounts Payable                 |  $16,335        | 
| Accrued Expenses                 |  $106,435       | 
| **Total Current Liabilities**    |  **$122,770**   | 

| Net Assets                         |                 | 
|------------------------------------|-----------------| 
| Unrestricted                       |  $1,186,690     | 
| Temporarily Restricted             |  $186,955       | 
| **Total Net Assets**               |  **$1,373,645** |
| _Total Liabilities and Net Assets_ | _$1,496,415_    |
