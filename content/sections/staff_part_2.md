#### Leadership

* ![John Sullivan](images/staff/john-sullivan_square_grad.jpg) **John Sullivan** Executive Director
* ![John Hsieh](images/staff/john-hsieh_square_grad.jpg) **John Hsieh** Deputy Director

#### Licensing

* ![Donald Robertson](images/staff/donald-robertson_square_grad.jpg) **Donald Robertson** Licensing and Compliance Manager
* ![Craig Topham](images/staff/craig-topham_square_grad.jpg) **Craig Topham** Copyright and Licensing Associate

#### Campaigns

* ![Molly de Blanc](images/staff/molly-de-blanc_square_grad.jpg) **Molly de Blanc** Campaigns Manager
* ![Dana Morgenstein](images/staff/dana-morgenstein_square_grad.jpg) **Dana Morgenstein** Outreach and Communications Coordinator

#### Tech

* ![Andrew Engelbrecht](images/staff/andrew-engelbrecht_square_grad.jpg) **Andrew Engelbrecht** Senior Systems Administrator
* ![Ian Kelling](images/staff/ian-kelling_square_grad.jpg) **Ian Kelling** Senior Systems Administrator
* ![Ruben Rodriguez](images/staff/ruben-rodriguez_square_grad.jpg) **Ruben Rodriguez** Chief Technology Officer

#### Operations

* ![Matt Lavallee](images/staff/matt-lavallee_square_grad.jpg) **Matt Lavallee** Operations Assistant
* ![](images/staff/no-photo.png) **Jeanne Rasata** Assistant to the President

<small>Leadership and staff lists are up to date as of publication, January 2019.</small>
