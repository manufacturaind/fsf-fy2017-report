Title: Donors

#### Institutional Support

* Private Internet Access  
* Alibaba Group  
* Craigslist Charitable Fund  
* 2A Foundation  
* Bloomberg  
* Google  
* Skowronski Family Foundation  
* Bibliotek-Systemer As
* IUT Béziers (Université Montpellier 2)  
* Purism  
* Audeo  
* GitHub  
* Open Invention Network  

#### In-Kind Support

* Bytemark  
* Markley Group  
* TowardEX  
* Technoethical  
* Aleph Objects  
* No Starch Press  
* ThinkPenguin  

#### $5000+

* Christian Frâncu    
* Julian Graham  
* Gregory Maxwell  
* James Wilson  

#### $1000+

* Aleph Objects, Inc  
* Jean-Francois Blavier  
* Alain Brenzikofer  
* Shawn C [a.k.a “citypw”]  
* Colin Carr  
* Antonio Carzaniga  
* Jeffrey Cliff  
* Steven Dick  
* Robert Dionne  
* Alexey Eromenko  
* Cătălin Frâncu  
* Matteo Frigo
* René Genz  
* Richard Harlow  
* Douglas Hauge  
* Matthias Herrmann
* Jonathan Howell  
* Stephen Ippolito  
* Brewster Kahle
* Donald and Jill Knuth
* Nikolay Ksenev
* Russell McManus
* Trevor Menagh
* Nebion AG
* Seungwon Park
* Sreeram Ramachandran
* Daniel Riek
* Peter Rock
* Luis Rodriguez
* Inouye Satoru
* Steve Sprang
* John Sullivan
* Puduvankunnil Udayakumar
* C&CZ IT Department, Faculty of Science, Radboud University
* Kat Walsh
* Philipp Weis
* Marinos Yannikos

#### $500-$999

* Jean-Louis Abraham
* Ben Abrams
* Bashar Al-Abdulhadi
* Xavier ALT
* Iñaki Arenaza in memory of Mr. Mauricio Saint-Supery
* Matthew Armstrong
* AskApache
* Salim Badakhchani
* Alexandre BLANC
* Blue Systems
* Mark Boenke
* Wade Brainerd
* Nicolae Carabut
* Alison Chaiken
* Conan Chiles
* Yidong Chong
* Judicaël Courant
* Donald Craig
* Allen Curtis
* Dangerous Thing
* Henrique Dante de Almeida
* Paul Eggert
* Markus Fischer
* Edward Flick
* Andrew Fox
* Arthur Gleckler
* Elyse Grasso
* Aaron Grothe
* Sam Halliday
* Steven Hay
* Michael Henderson
* Håkon A. Hjortland
* Brett Holleman
* Daniel Hoodin
* Clifford Ireland
* Martin Jansche
* Christian Johansen
* Uday Kale
* Chase Kelley
* David Klann
* Colin Klingman
* Adam Klotblixt
* Warren Knight 
* Øyvind Gard Knudtzen
* Martin Krafft
* Peter Kunze
* Adam Lewis
* Morten Lind
* Denis López
* Shyama Mandal
* Christopher Marusich
* Miromico AG
* David Moews
* Kyohei Moriyama
* Bill Newcomb
* Pablo Adrian Nieto
* Freddie O'Connell
* jeffrey oconnell
* Stephanie Ogden
* Marcus Pemer
* Donnie Pennington
* Roland Pesch
* Valerio Poggi
* David Potter
* Nicolas Pottier
* Ed Price
* Vivek Ramachandran
* Norman Richards
* francisco rodriguez
* Tyler Romeo
* Leah Rowe
* Sean Russell
* Minoru Sekine
* Ben Simmonds
* Bijan Soleymani
* Trevor Spiteri
* Gary Stimson
* 悟高田
* Micah Tombli
* David Turner
* Rob Vens
* Spencer Visick
* Paul Wang
* ivo Welch
* Eric West
* Jim Wright
* hiroo yamagata
* Adam Ymeren

<small>This list includes our patrons, in-kind supporters, and those who receive
ThankGNUs for donations totaling over $500 in a year. The FSF appreciates and
thanks the thousands of individual donors, members, and corporate patrons
worldwide who make our work possible.</small>
