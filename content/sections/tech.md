Title: Tech
PullQuote: The FSF has upgraded free software development infrastructure across the board.
Image: images/sysadmin-libreplanet.jpg
ImageAlt: A tiered classroom, filled with people. A person stands at the front of the room, beneath a large projection screen, giving a talk.
ImageCaption: The tech team maintains the free software infrastructure for all FSF operations, including the annual LibrePlanet conference.
ImageCredit: Credit: Kori Feener, CC-BY 4.0

## Infrastructure for free software development and activism

This year, the FSF's tech team made some crucial infrastructure
upgrades that improve both the Foundation's daily operations and the
[GNU Project](https://www.gnu.org/). The changes included running
more services on hardware that is Respects Your Freedom certified, including a Librebooted BIOS and running Trisquel GNU/Linux, proving that complex
software projects and modern nonprofit organizations can succeed relying on free software. 

The tech team and its volunteers also power LibrePlanet, the
annual free software conference. Its livestream and recording
infrastructure reduce barriers to access for those who are not able to
attend the conference in person. Volunteer-monitored IRC channels for
each talk and workshop room increase the opportunity to contribute to
the conversation. Recordings are archived on the FSF's [GNU
MediaGoblin instance](https://media.libreplanet.org/) shortly after
the event. And it's all done with free software!
