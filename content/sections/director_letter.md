Title: Letter from the Executive Director
Image: images/staff/john-sullivan.jpg
PullQuote: Software filters the information we receive about the world and the messages we put out into the world, and that software must be free "as in freedom."

**Dear Supporters,**

As I write this, reflecting on our previous financial year,
discussions centered on our rights and our fears while using
technology are all over the mainstream news. Facebook is being called
before governments in the US and Europe to explain itself. Uber and
Tesla are under fire for their software-driven cars killing
people. Investigations continue into how criminals may have broken
into voting machines and used online news platforms to influence
elections in the US and elsewhere around the world. Pundits say people
are more divided than ever on "the issues," and that we need to
actualize the theoretical capability of electronic communication to
bridge differences and bring people together.

The common but silent thread connecting these discussions is the power
of software. Software filters the information we receive about the
world, the messages we put out into the world, and even the way we
physically move in the world. If the software is not free "as in
freedom," so that anyone can inspect, modify, and share it, these
filters will inevitably be keyed to the interests of the gatekeepers
who control it. The consequences for the rest of us will be loss of
democracy, privacy, security, freedom of speech, freedom of movement
— and even loss of life. And yet, the ideals of the free software
movement have been largely suppressed in the global conversation about
avoiding technological dystopia.

The FSF is changing that. The audited FY2017 financial statement
contained in this report continues the solid good news you've come to
expect from us — the steady growth and transparent, mission-centric
use of funds that have earned the FSF an exceptionally high [Charity
Navigator
rating](https://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=8557)
of 97.15 out of 100.

In these pages, you'll see how we've been expanding the free software
movement and amplifying your principled voice on the international
stage, accelerating development of the GNU operating system and other
free software, inspecting and certifying freedom-respecting products
to incentivize needed change in the commercial sector, and
strengthening copyleft licensing — still the best tool we have to
establish a culture where technology empowers rather than oppresses. I
hope that these program updates from our campaigns, technical,
licensing, and operations teams inspire you to step up your own efforts
toward our shared goals.

We need everyone not only using free software in their daily lives but
also seeing free software as their political, philosophical issue. We
will eventually reach a tipping point, where awareness of the Four
Freedoms will be sufficiently ingrained in policymakers,
technologists, and the public, such that any new enterprise that does
not respect them will meet quickly with failure. But we have a long
way to go, and delays in getting there have real, human costs.

FSF associate members drive this work, through their ongoing financial
contributions, their commitment to the movement, and their sustained,
informed, effective personal advocacy. If you haven't joined us yet,
we need your support. We may be up against billions of dollars, but
since we are fighting for billions of regular users, we have a massive
advantage in numbers — so long as we can reach everyone. Please join
us and help build these freedom-protecting defenses, without delay.

Yours in freedom,  
<img alt="signature" class="signature" src="images/JohnS-signature.png">

* John Sullivan
* Executive Director
* September 2018
