We championed copylefted projects, publishing another seven installments in
an [ongoing series of conversations with free software developers who choose GNU licenses for their work](https://www.fsf.org/blogs/licensing). We also certified a record number of devices under
our [Respects Your Freedom](https://www.fsf.org/ryf) certification program,
which identifies devices that do as much as possible to respect user freedom.

Along with answering hundreds of licensing questions from the public, dedicated
volunteers continued to expand and improve the [Free Software
Directory](https://directory.fsf.org/)
for nearly two million annual visitors, and we made inroads with government,
helping the US Department of Defense to distribute free software. We also
organized volunteers to [create a workaround
for](https://www.fsf.org/blogs/licensing/you-can-now-register-as-a-dmca-agent-without-using-nonfree-javascript)
Web site maintainers who want to register as an agent under the US Digital
Millennium Copyright Act (DMCA) while avoiding the nonfree JavaScript the
process currently requires.

