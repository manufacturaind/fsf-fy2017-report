Title: Preamble

The Free Software Foundation is a leader in the international movement
for computer user freedom. We defend the rights of all software users
and encourage the development and use of free _["as in freedom"](https://www.gnu.org/philosophy/free-sw.en.html)_
software.

This annual report highlights the Foundation's activities and
achievements in fiscal year 2017 (October 1, 2016 — September 30,
2017) and includes a detailed financial statement.
