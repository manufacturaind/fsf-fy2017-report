Title: Associate Membership

### Join the Free Software Foundation

Our mission is to promote computer user freedom and defend the rights
of all software users, worldwide. Though free software is used more
widely than ever, it is under threat from a wide range of interests
that profit from controlling and surveilling computer users. You can
help put control over computers back in the hands of the people who
use them! Try [a free software
program](https://directory.fsf.org/wiki/Free_Software_Directory:Free_software_replacements),
or [switch to a free operating
system](https://www.gnu.org/distros/free-distros.html). [Make a
donation](https://my.fsf.org/donate), or make a long term commitment
to free software by [becoming a Free Software Foundation Associate
Member](https://my.fsf.org/join). Learn more at [fsf.org](https://fsf.org).
