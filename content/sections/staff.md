Title: Leadership and Staff

#### Board of Directors

* ![Richard M. Stallman](images/staff/richard-stallman_grad.jpg) **Richard M. Stallman** Founder and President
* **Gerald J. Sussman** Professor of Electrical Engineering, MIT
* **Geoffrey Knauth** Senior Software Developer at AccuWeather
* **Henry Poole** Founder, CivicActions
* **Benjamin Mako Hill** Assistant Professor of Communications at the University of Washington
* **Bradley M. Kuhn** President and Distinguished Technologist, Software Freedom Conservancy
* **Kat Walsh** Attorney
