### Working Together for Free Software

<ul>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=50" title="GNU Guix"><img src="images/project-logos/guix.svg" alt="GNU Guix"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=22" title="GNU Mailman"><img src="images/project-logos/mailman.svg" alt="GNU Mailman"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=36"title="GNU MediaGoblin"><img src="images/project-logos/mediagoblin.svg" alt="GNU MediaGoblin"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=10"title="GNU Octave"><img src="images/project-logos/octave.svg" alt="GNU Octave"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=16"title="GNU Radio"><img src="images/project-logos/gnuradio.svg" alt="GNU Radio"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57"title="GNU Toolchain"><img src="images/project-logos/gnu-toolchain.svg" alt="GNU Toolchain"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=19"title="Replicant"><img src="images/project-logos/replicant.svg" alt="Replicant"></a></li>
<li><a href="https://my.fsf.org/civicrm/contribute/transact?reset=1&id=58"title="SeaGL"><img src="images/project-logos/seagl.png" alt="SeaGL"></a></li>
</ul>

The FSF acts as fiscal sponsor for free software projects and events whose scope
and purpose align with the FSF’s mission. Participating projects
benefit from the FSF’s nonprofit status, administrative
infrastructure, and fundraising expertise. In FY17, these eight
projects received $67,130 in direct support from the free software
community via the Working Together for Free Software Fund.

