Additionally, this three-person team runs the GNU shell server, oversees the
security of GNU software distribution and infrastructure, and works with
volunteers to maintain [savannah.gnu.org](https://savannah.gnu.org). They also
maintain many services used by staff, volunteers, and the free software
community, including the FSF's CiviCRM server, fsf.org, libreplanet.org,
defectivebydesign.org, the Free Software Directory, XMPP servers for staff and
members, mailing lists, apt mirrors, git repositories, internal wikis, a GNU
social server, a Single Sign-On server, email servers, DNS servers, internal
site monitoring systems, a Request Tracker instance, and the FSF shop. 

And the team collaborates with the free software community, maintaining gnu.org
with the help of volunteers, and working with interns who are advancing their
skills and knowledge in working with and creating free software.
