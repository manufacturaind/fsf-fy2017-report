Title: FY2017 Financial Statement 
OpeningText: The following is a visualization of the Free Software Foundation's FY 2017 financial statement. The original documents can be found <a href="https://www.fsf.org/about/financial" target="_blank">here</a>.

### Statement of Financial Position

| Assets                                    |               | 
|-------------------------------------------|---------------| 
| Cash and Cash Equivalents                 |   $1,191,910  | 
| Accounts Receivable and Inventory         |   $28,425     | 
| Prepaid Expenses and Other Current Assets |   $4,038      | 
| Investments                               |   $239,645    | 
| Property, Equipment                       |   $19,169     | 
| Non-Current Assets                        |   $13,228     | 
| _Total Assets_                            |  _$1,496,415_ | 
