Title: Licensing and Compliance
PullQuote: We champion copylefted projects and developers who choose GNU licenses for their work.
Image: images/licensing-gingerich_crop.jpg
ImageAlt: Photo of a man with light brown hair, glasses, and a brown, white, and red sweater, holding two small mobile phones in his hands.
ImageCaption: Denver Gingerich, a developer of packages under the AGPLv3 and other GNU licenses, speaking at LibrePlanet 2017 about free software for mobile phones.
ImageCredit: Credit: Kori Feener, CC-BY 4.0

The FSF's [Licensing and Compliance Lab](https://www.fsf.org/licensing/)
defends free software through license enforcement and enforcement support, a
rigorous product certification, and educational resources.

In FY17, we celebrated the tenth anniversary of the [GNU General Public License
version 3 (GPL)](https://www.gnu.org/licenses/gpl.html). This is the free
software copyleft license that ensures the user's right to run, study, share,
and modify software. In addition to being the steward of the GPL, the FSF holds copyright on much of the GNU operating system. The licensing team spent the year accepting copyright assignments
from software developers and corporations, investigating GPL violations, and
answering licensing questions from the community.
