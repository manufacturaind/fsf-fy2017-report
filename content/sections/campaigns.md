Title: Campaigns
Image: images/campaigns-drm.png
ImageAlt: A photo of a wall in Florida. Spraypainted on the wall is 'DRM' with a red circle and crossed out.
ImageCaption: 34th Street Wall, Gainesville, Florida
ImageCredit: Credit: Gavin Baker, CC-BY-SA 4.0
SubTitle: Fighting DRM wherever it's found
OpeningText: In the past year, we've fought back against <a href="https://www.defectivebydesign.org/what_is_drm_digital_restrictions_management">Digital Restrictions Management (DRM)</a> on multiple fronts.

### International Day Against DRM

On July 9, 2017, [International Day Against DRM
(IDAD)](https://www.defectivebydesign.org/dayagainstdrm/2017) focused on Web-based
community involvement, with more than twenty organizations raising their voices in
support of a world without DRM. From blog posts to special sales, to parties and
movie showings around the world, people came out to celebrate DRM-free media,
and raise awareness of the threats we face from DRM.

### DMCA exemptions

The licensing team continued to raise the alarm about legislative issues like 
the US federal government's continued insistence on [forcing supporters
of user freedom to fight for exemptions to the Digital Millennium
Copyright Act's (DMCA) anti-circumvention
restrictions](https://www.defectivebydesign.org/blog/tell_us_copyright_office_dmca_anticircumvention_rules_are_broken). 

The DMCA is a particularly egregious example of legislative endorsement of DRM.
It destroys user freedom, and concentrates control over the production and distribution of digital media,
giving DRM peddlers the power to carry out massive digital book burnings and
conduct large scale surveillance over people's media viewing habits. Even the
process for getting exceptions to the anti-circumvention rules for the purposes
of research or use of assistive technologies is draconian, resetting every
three years and requiring nonfree JavaScript in order to submit a comment on
the process, unless you get special permission to comment in another way.

Along with our own comments, we rallied the free software community to
submit their own comments in favor of anti-circumvention exemptions —
and passionate critiques of the entire process.
