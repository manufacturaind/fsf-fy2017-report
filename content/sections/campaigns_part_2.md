### EME in Web standards

Encrypted Media Extensions (EME) is just another way to dress up DRM. The FSF and the free software community organized to oppose a proposal by the World Wide Web Consortium (W3C) to make EME a Web standard.  We called and wrote W3C president Tim Berners-Lee, asking him to keep the Web free. [While we lost this battle](https://www.defectivebydesign.org/blog/tim_bernerslee_approves_web_drm_w3c_member_organizations_have_two_weeks_appeal), and EME became a Web standard, we're looking at our options for next steps. We are not giving up hope for a free Web, even if its inventor did.

## Free people, free net

Internet freedom in the United States found itself on the national
stage in 2017. Over the course of the year, Ajit Pai became chairman
of the Federal Communications Commission (FCC) and instigated a war
against net neutrality. The FSF [joined with dozens of other
organizations to raise awareness and organize in support of net
neutrality](https://www.fsf.org/blogs/community/today-july-12th-day-of-action-for-net-neutrality).
