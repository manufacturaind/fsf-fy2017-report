Title: Cover
Box1Title: Licensing
Box1Text: Educating you about free software licenses, advocating for copyleft and enforcing the GNU General Public License
Box2Title: Campaigns
Box2Text: Empowering you to understand, adopt, develop, and defend free software
Box3Title: Tech
Box3Text: Providing infrastructure to accelerate development and distribution of the free software you need
Box4Title: Operations
Box4Text: Running an efficient, effective nonprofit, using free software, supported by you

# FY2017 Annual Report

**Highlighting activities and detailed financials for Fiscal Year 2017**  
(October 1, 2016 - September 30, 2017)
