<?php
/*
Template Name: Page - With Sections
*/
?>
<?php 
  get_header(); 
  $show_sidebar=$prk_samba_frontend_options['right_sidebar'];
  if ($show_sidebar=="yes")
    $show_sidebar=true;
  else
    $show_sidebar=false;
  $data = get_post_meta( $post->ID, '_custom_meta_theme_page', true );
  $show_title=true;
  $show_slider=false;
  if (!empty($data))
  {
    if (isset($data['alchemy_show_sidebar']) && $data['alchemy_show_sidebar']=="yes") {
      $show_sidebar=true;
    }
    if (isset($data['alchemy_show_sidebar']) && $data['alchemy_show_sidebar']=="no") {
      $show_sidebar=false;
    }
    if (isset($data['alchemy_show_title']) && $data['alchemy_show_title']=="yes") {
        $show_title=false;
    }
    if (isset($data['alchemy_featured_slide'])) {
        $show_slider=$data['alchemy_featured_slide'];
    }
    if (isset($data['alchemy_full_slide']) && $data['alchemy_full_slide']!="")
      $autoplay="true";
    else
      $autoplay="false";
    if (isset($data['alchemy_full_delay']) && $data['alchemy_full_delay']!="")
      $delay=$data['alchemy_full_delay'];
    else
      $delay=$prk_samba_frontend_options['delay_portfolio'];
    $inside_filter="";
    $in_flag=false;
    foreach ($data as $childs)
    {
      //ADD THE CATEGORIES TO THE FILTER
      if ($in_flag==true)
      { 
        $inside_filter.=$childs.", ";
      }
      if ($childs=='weirdostf')
        $in_flag=true;
    }
    }
  //NEVER SHOW SIDEBAR
  $show_sidebar=false;
?>
<div id="centered_block">
<div id="main_block" class="row block_with_sections page-<?php echo get_the_ID(); ?>">
    <?php
      echo prk_output_featured_image(get_the_ID());
      if ($show_title==true)
      {
          prk_output_title($data);
          $extra_class=" with_title";
      }
      else
      {
        $extra_class="";
      }
    ?>
    <div id="content">
        <div id="main" role="main" class="main_with_sections<?php echo $extra_class; ?>">
            <?php
                if ($show_slider=="yes")
                {
                    echo do_shortcode('[prk_slider id="samba_slider-'.get_the_ID().'" category="'.$inside_filter.'" autoplay="'.$autoplay.'" delay="'.$delay.'" sl_size=""]');
                }
                if ($show_slider=="show_revol")
                {
                  echo '<div class="prk_rv">'; 
                    echo do_shortcode('[rev_slider '.$data['alchemy_revslider'].']');
                  echo '</div>';
                }
                if ($show_sidebar)
                {
                  echo '<div class="twelve sidebarized">';
                }
                else
                {
                  echo '<div class="twelve">'; 
                }
                while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
                <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
                <div class="clearfix"></div>
                </div>
              <?php endwhile; /* End loop */ ?>
            <?php 
              if ($show_sidebar) 
              {
                  ?>
                <aside id="sidebar" class="<?php echo SIDEBAR_CLASSES; ?> inside right_floated top_15" role="complementary">
                    <?php get_sidebar(); ?>
                </aside><!-- /#sidebar -->
                </div>
                <?php
              }
            ?>
            <div class="clearfix"></div>
        </div><!-- /#main -->
    </div><!-- /#content -->
</div><!-- #main_block -->
</div>

<!-- pt hello -->


    <div id="sticky" class="modal">
        <div id="modal-wrapper">
            <div class="modal-left">
                <img src="http://www.idmais.org/wp-content/uploads/2018/09/idmais-vertical.png" alt="ID+, Research Institute for Design, Media and Culture">
            </div>
            <div class="modal-right">
              <h3>
                Our website is currently being renovated. <br>
                Please enter your email here to be notified when the new site goes live.
              </h3>
              <form action="https://formspree.io/idmais@gmail.com" method="post" target="_blank" novalidate="">
                  <div class="input-group">
                    <input type="text" name="email" placeholder="Email">
                    <div class="input-group-button">
                      <input name="update-notification" class="button" value="Send" type="submit">
                    </div>
                  </div>
              </form>
              
              <h3>Meanwhile, you can visit the previous <a href="http://www.idmais.org/pt-pt" rel="modal:close">ID+ website here</a>.
             </h3>
           </div>
       </div>
    </div>

    <!-- jQuery Modal -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    <style id='new-css' type='text/css'>
        .blocker {
            background-color: #000;
            background-color: rgba(0,0,0,1);
        }
        .blocker #sticky {
            padding: 4em;
            max-width: calc(70% - 8em);
            border-radius: 0;
        }
        
        #modal-wrapper {
            margin-left: auto;
            margin-right: auto;
            display: flex;
            flex-direction: row;
        }
        
        @media screen and (max-width: 39.9375em) { /* Small only */
            .blocker #sticky {
                padding: 2em;
                max-width: calc(100% - 4em);
            }
            #modal-wrapper {
                display: block;
            }
             .blocker #sticky img {
                height: 240px;
                margin-right: 0;
                margin-bottom: 2em;
            }
          }
          
        .blocker #sticky h3 {
            margin-bottom: 1.5em;
        }
          
        .blocker #sticky h3 br {
            display: block;
            margin-bottom: 0.5em;
        }
        
        .blocker #sticky img {
            display: block;
            width: auto;
            height: 100%;
            max-height: 340px;
            vertical-align: middle;
            margin-right: 4em;
        }
        
        .blocker #sticky h3,
        .blocker #sticky p,
        .blocker #sticky form {
            max-width: 600px;
        }
        
        .blocker #sticky form {
            margin-top: 1em;
            margin-bottom: 2em;
        }
        
        
        .blocker #sticky form input::placeholder, textarea::placeholder {
            color: #cacaca;
        }
        .blocker #sticky form input[type="text"],
        .blocker #sticky form input[type="email"] {
            display: block;
            box-sizing: border-box;
            width: 100%;
            height: 2.4375rem;
            margin: 0 0 1rem;
            padding: 0.5rem;
            border: 1px solid #cacaca;
            border-radius: 3px;
            background-color: #fefefe;
            box-shadow: inset 0 1px 2px rgba(10, 10, 10, 0.1);
            font-family: inherit;
            font-size: 1rem;
            font-weight: normal;
            line-height: 1.5;
            color: #67605e;
            transition: box-shadow 0.5s, border-color 0.25s ease-in-out;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        
        .blocker #sticky form button::-moz-focus-inner,
        .blocker #sticky form input[type="button"]::-moz-focus-inner,
        .blocker #sticky form input[type="submit"]::-moz-focus-inner {
            border-style: none;
            padding: 0;
        }

        .blocker #sticky form .button {
            display: block;
            width: 100%;
            vertical-align: middle;
            margin: 0 0 1rem 0;
            font-family: inherit;
            padding: 0.85em 1em;
            -webkit-appearance: none;
            border: 1px solid transparent;
            border-radius: 3px;
            transition: background-color 0.25s ease-out, color 0.25s ease-out;
            font-size: 0.9rem;
            line-height: 1;
            text-align: center;
            cursor: pointer;
            background-color: #67605e;
            color: #fefefe;
        }

        .blocker #sticky form input[type="submit"],
        .blocker #sticky form input[type="button"] {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 3px;
        }
        
        .blocker #sticky form .button:hover,
        .blocker #sticky form .button:focus {
            background-color: #f6911d;
            color: #fefefe;
        }
        
        #sticky a.close-modal { display: none; }
    </style>
    <script type="text/javascript">
      jQuery(document).ready( function () {
        console.log('document ready');
        jQuery('#sticky').appendTo('body').modal({
          escapeClose: false,
          clickClose: false,
          showClose: false        
        });
      });
    </script>

<?php get_footer(); ?>
