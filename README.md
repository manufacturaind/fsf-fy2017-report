This repository contains the source files for both the web and print versions of the Free Software Foundation's FY2017 Report.

The actual report can be [read online](https://www.fsf.org/annual-reports/fy2017/) or as a [print-ready PDF](https://www.fsf.org/annual-reports/fy2017/print-fsf-fy2017.pdf). See also the [release announcement](https://www.fsf.org/news/free-software-foundation-fy2017-annual-report-now-available).

# Contents

* `content/sections/` -- formatted Markdown content for each section
* `content/images/` -- image media
* `static/` -- static assets (CSS, fonts, etc)
* `print` -- the Scribus files for the print version of the report

# Libraries and assets

* [dart-sass](https://github.com/sass/dart-sass/) for compiling SCSS into CSS
* [Jinja](http://jinja.pocoo.org/) for compiling Markdown content using HTML templates

See each library's link for its licensing terms.

## Generating the site

Clone the repository and run `make install && make build`. This will create a new folder (called dist) that contains all the files for the website. 

## Fonts

All the fonts used here are available under the [SIL Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

* [Hanken Grotesk](http://usemodify.com/fonts/hk-grotesk/) by Alfredo Marco Pradil
* [Karla](https://fonts.google.com/specimen/Karla) by Jonny Pinhorn

# License

The source code for the website is made available under the [GPL (version 3 or later)](https://www.gnu.org/licenses/license-list.en.html#GNUGPLv3).

The source files for the print version, as well as the visual assets for both the website and print reports (excluding photos) are made available under the [Creative Commons Attribution-ShareAlike license](https://www.gnu.org/licenses/license-list.en.html#ccbysa) (CC-BY-SA 4.0).

See the report itself for the licensing terms of the included photos.
